<?php

namespace App\Utils;

use Carbon\Carbon;

class Helper
{

    /**
     * Parse string hour to Carbon hour
     *
     * @param string $hour
     * @return void
     */
    public function getHour($hour): string
    {
        return Carbon::parse($hour)->format('H');
    }

    /**
     * Parse string date to Carbon date
     *
     * @param string $date
     * @return void
     */
    public function getDate($date): Carbon
    {
        return Carbon::parse($date)->locale('fr_FR');
    }
}
