<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Feature;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class FeatureFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        

        for ($i = 1; $i <= 20; $i++) {
            $feature = new Feature();
            $room = $this->getReference('room'.$i);
            $feature->setName($faker->word());
            $feature->addRoom($room);
            $manager->persist($feature);
            
        }
        $manager->flush();
    }
}
