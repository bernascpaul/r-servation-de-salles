<?php

namespace App\DataFixtures;

use Faker\Factory;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\Room;
use App\Entity\Booking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BookingFixture extends Fixture  implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return [RoomFixture::class];
    }

    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();
        
        
        
        for ($i = 1; $i <= 20; $i++) {
            $room = $this->getReference('room'.$i);
            $date =$faker->date($format = 'Y-m-d', $min = 'now');
            $openHour = $faker->time($format = 'H:i:s', $max = 'now');
            $closeHour = $faker->time($format = 'H:i:s', $min = 'now');
            $booking = new Booking();
            $booking->setUser($faker->name())
                    ->setName($faker->company())
                    ->setMessage($faker->text)
                    ->setRoom($room)
                    ->setDate(\DateTime::createFromFormat('Y-m-d', $date))
                    ->setOpeningHour(\DateTime::createFromFormat('H:i:s', $openHour))
                    ->setClosingHour(\DateTime::createFromFormat('H:i:s', $closeHour));
            $manager->persist($booking);
        }
        $manager->flush();
    }
}