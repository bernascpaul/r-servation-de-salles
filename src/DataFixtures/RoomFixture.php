<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Booking;
use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RoomFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();
        

        for ($i = 1; $i <= 20; $i++) {
            $room = new Room();
            $this->addReference('room'.$i, $room);
            $room->setName($faker->company())
                    ->setFloor($faker->numberBetween($min = 0, $max = 3))
                    ->setCapacity($faker->numberBetween($min = 10, $max = 40))
                    ->setBuilding($faker->secondaryAddress);
            $manager->persist($room);
            
        }
        $manager->flush();
    }
}
