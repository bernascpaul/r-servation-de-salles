<?php

namespace App\DataFixtures;

use App\Entity\Params;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ParamsFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $feature = new Params();
        $feature->setKeyName('lundi');
        $feature->setValueName('1');
        $manager->persist($feature);

        $feature2 = new Params();
        $feature2->setKeyName('mardi');
        $feature2->setValueName('1');
        $manager->persist($feature2);

        $feature3 = new Params();
        $feature3->setKeyName('mercredi');
        $feature3->setValueName('1');
        $manager->persist($feature3);

        $feature4 = new Params();
        $feature4->setKeyName('jeudi');
        $feature4->setValueName('1');
        $manager->persist($feature4);

        $feature5 = new Params();
        $feature5->setKeyName('vendredi');
        $feature5->setValueName('1');
        $manager->persist($feature5);

        $feature6 = new Params();
        $feature6->setKeyName('samedi');
        $feature6->setValueName('0');
        $manager->persist($feature6);

        $feature7 = new Params();
        $feature7->setKeyName('dimanche');
        $feature7->setValueName('0');
        $manager->persist($feature7);

        $feature8 = new Params();
        $feature8->setKeyName('heure_ouverture');
        $feature8->setValueName('8');
        $manager->persist($feature8);

        $feature9 = new Params();
        $feature9->setKeyName('heure_fermeture');
        $feature9->setValueName('19');
        $manager->persist($feature9);

        $manager->flush();
    }
}
