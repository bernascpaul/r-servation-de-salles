<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

const API_URL = 'https://sso.lpweb-lannion.fr/api/v1.7/';

class Auth
{
    public static function login()
    {
        $request = new Request();

        $username = $request->request->get('username');
        $password = $request->request->get('password');

        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', API_URL . 'login?user=' . $username . '&password=' . $password);
        $response = $response->toArray();
        if ($response['ret']) {
            $session = new Session();
            $session->set('token', $response['token']);

        } else {
            return new JsonResponse('Vous n\'êtes pas authorizé à accèder à ce module', Response::HTTP_OK, [], true);
        }

    }

    public static function checkAccess($token)
    {
        $authUrl = 'https://sso.lpweb-lannion.fr/api/v1.7/';
        $httpClient = HttpClient::create();
        $response = $httpClient->request('GET', $authUrl . 'check_access/salles?token=' . $token);
        $response = $response->toArray();

        return $response['ret'] === 'granted' ? true : false;
    }
}
