<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParamsRepository")
 */
class Params
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"params:simple"})
     */
    private $keyName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"params:simple"})
     */
    private $valueName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeyName(): ?string
    {
        return $this->keyName;
    }

    public function setKeyName(string $keyName): self
    {
        $this->keyName = $keyName;

        return $this;
    }

    public function getValueName(): ?string
    {
        return $this->valueName;
    }

    public function setValueName(string $valueName): self
    {
        $this->valueName = $valueName;

        return $this;
    }
}
