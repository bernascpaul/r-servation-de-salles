<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"booking:full","booking:simple"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"booking:full","booking:simple"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="bookings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"booking:full","booking:simple"})
     * @Assert\NotBlank
     */
    private $room;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"booking:full","booking:simple"})
     */
    private $user;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @Groups({"booking:full","booking:simple"})
     */
    private $guests = [];

    /**
     * @ORM\Column(type="date")
     * @Groups({"booking:full","booking:simple"})
     */
    private $date;

    /**
     * @ORM\Column(type="time")
     * @Groups({"booking:full","booking:simple"})
     */
    private $openingHour;

    /**
     * @ORM\Column(type="time")
     * @Groups({"booking:full","booking:simple"})
     */
    private $closingHour;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"booking:full","booking:simple"})
     */
    private $message;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(string $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getGuests(): ?array
    {
        return $this->guests;
    }

    public function setGuests(?array $guests): self
    {
        $this->guests = $guests;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getOpeningHour(): ?\DateTimeInterface
    {
        return $this->openingHour;
    }

    public function setOpeningHour(\DateTimeInterface $openingHour): self
    {
        $this->openingHour = $openingHour;

        return $this;
    }

    public function getClosingHour(): ?\DateTimeInterface
    {
        return $this->closingHour;
    }

    public function setClosingHour(\DateTimeInterface $closingHour): self
    {
        $this->closingHour = $closingHour;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->id;
    }
}
