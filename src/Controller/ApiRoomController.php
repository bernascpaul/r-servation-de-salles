<?php

namespace App\Controller;

use App\Entity\Room;
use App\Repository\RoomRepository;
use App\Service\Auth;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiRoomController extends AbstractController
{
    /**
     * @Route("/api/room", name="api_room", methods={"GET"})
     */
    public function index(Request $request, RoomRepository $roomRepository, SerializerInterface $serializerInterface)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $rooms = $roomRepository->findAll();
        $result = $serializerInterface->serialize($rooms, 'json', [
            'groups' => ['room:read'],
        ]);

        return new JsonResponse($result, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/room/{id}", name="api_room_getById", methods={"GET"})
     */
    public function getById(Request $request, Room $room, SerializerInterface $serializer)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $data = $serializer->serialize($room, 'json', [
            'groups' => ['room:read'],
        ]);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/room", name="api_room_create", methods={"POST"})
     */
    public function createRoom(Request $request, RoomRepository $roomRepository, DecoderInterface $decoderInterface, SerializerInterface $serializerInterface, EntityManagerInterface $manager, ValidatorInterface $validatorInterface)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $data = $request->getContent();
        $dataTab = $decoderInterface->decode($data, 'json');

        $room = new Room();

        $nameRoom = $dataTab['nameroom'];
        $room->setName($nameRoom);
        $floorRoom = $dataTab['floorroom'];
        $room->setFloor($floorRoom);
        $buildingRoom = $dataTab['buildingroom'];
        $room->setBuilding($buildingRoom);
        $capacity = $dataTab['capacity'];
        $room->setCapacity($capacity);

        $errors = $validatorInterface->validate($room);
        if (count($errors)) {
            $errorsJson = $serializerInterface->serialize($errors, 'json');
            return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
        }

        $manager->persist($room);
        $manager->flush();

        return $this->json($room, Response::HTTP_CREATED, [], ['groups' => 'room:read']);

    }

    /**
     * @Route("/api/room/{id}", name="api_room_update", methods={"POST"})
     */
    public function UpdateRoom(Request $request, Room $room, DecoderInterface $decoderInterface, SerializerInterface $serializerInterface, EntityManagerInterface $manager, ValidatorInterface $validatorInterface)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $data = $request->getContent();
        $dataTab = $decoderInterface->decode($data, 'json');

        if (array_key_exists('nameroom', $dataTab)) {
            $nameRoom = $dataTab['nameroom'];
            $room->setName($nameRoom);
        }

        if (array_key_exists('floorroom', $dataTab)) {
            $floorRoom = $dataTab['floorroom'];
            $room->setFloor($floorRoom);
        }

        if (array_key_exists('buildingroom', $dataTab)) {
            $buildingRoom = $dataTab['buildingroom'];
            $room->setBuilding($buildingRoom);
        }

        if (array_key_exists('capacity', $dataTab)) {
            $capacity = $dataTab['capacity'];
            $room->setCapacity($capacity);
        }

        $errors = $validatorInterface->validate($room);
        if (count($errors)) {
            $errorsJson = $serializerInterface->serialize($errors, 'json');
            return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
        }

        $manager->persist($room);
        $manager->flush();

        return $this->json($room, Response::HTTP_CREATED, [], ['groups' => 'room:read']);

    }

    /**
     * @Route("/api/room/{id}", name="api.room.delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function delete(Request $request, Room $room, EntityManagerInterface $manager)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $manager->remove($room);
        $manager->flush();

        return new JsonResponse("La salle a bien été supprimé", Response::HTTP_OK, [], false);
    }
}
