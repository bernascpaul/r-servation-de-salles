<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Repository\BookingRepository;
use App\Repository\ParamsRepository;
use App\Repository\RoomRepository;
use App\Service\Auth;
use App\Utils\Helper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiBookingController extends AbstractController
{
    /**
     * @Route("/api/booking", name="api.booking.getall", methods={"GET"})
     */
    public function getAll(Request $request, BookingRepository $bookingRepository, SerializerInterface $serializerInterface)
    {
        $token = $request->query->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], false);
        }

        $bookings = $bookingRepository->findAll();

        if (empty($bookings)) {
            return new JsonResponse('Aucune réservation n\'a été trouvé', Response::HTTP_NOT_FOUND, [], false);
        }

        $result = $serializerInterface->serialize($bookings, 'json', [
            'groups' => ['booking:simple'],
        ]);

        return new JsonResponse($result, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/booking/{id}", name="api.booking.getbyid", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function getById(Request $request, Booking $booking, SerializerInterface $serializer)
    {
        $token = $request->query->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], false);
        }

        $data = $serializer->serialize($booking, 'json', [
            'groups' => ['booking:full'],
        ]);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/booking", name="api.booking.create", methods={"POST"})
     */
    public function create(Request $request, BookingRepository $bookingRepository, ParamsRepository $paramsRepository, RoomRepository $roomRepository, DecoderInterface $decoderInterface, SerializerInterface $serializerInterface, EntityManagerInterface $manager, ValidatorInterface $validatorInterface)
    {
        $token = $request->query->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], false);
        }

        $data = $request->getContent();
        $dataTab = $decoderInterface->decode($data, 'json');

        // Check if day is valid

        $date = Helper::getDate($dataTab['date']);
        $day = $date->dayName;
        $dayInParams = $paramsRepository->findOneBy(['keyName' => $day]);

        if ($dayInParams->getValueName() === (string) 1) {

            // Check if hours are valid
            $openingHour = Helper::getHour($dataTab['openingHour']);
            $closingHour = Helper::getHour($dataTab['closingHour']);
            $openingHourInParams = $paramsRepository->findOneBy(['keyName' => 'heure_ouverture']);
            $closingHourInParams = $paramsRepository->findOneBy(['keyName' => 'heure_fermeture']);

            if (($openingHour >= $openingHourInParams->getValueName() && $closingHour <= $closingHourInParams->getValueName()) && $openingHour < $closingHour) {

                // Check if room is available

                $available = 0;
                $bookings = $bookingRepository->findAll();

                foreach ($bookings as $value) {

                    if ($value->getRoom()->getId() === (int) $dataTab['room']) {

                        $roomDate = Helper::getDate($value->getDate());

                        // Check if dates are equal

                        if ($date->equalTo($roomDate)) {

                            $roomOpeningHour = Helper::getHour($value->getOpeningHour());
                            $roomClosingHour = Helper::getHour($value->getClosingHour());

                            if ((($roomOpeningHour <= $openingHour) && ($openingHour <= $roomClosingHour)) || (($roomOpeningHour <= $closingHour) && ($closingHour <= $roomClosingHour))) {
                                $available++;
                                return new JsonResponse('La salle est déjà réservée sur ce créneau.',
                                    Response::HTTP_BAD_REQUEST, [], false);
                            }
                        }
                    }
                }

            } else {
                return new JsonResponse('La salle n\'est pas disponible sur ce créneau : Ouverture à ' . $openingHourInParams->getValueName() . 'H et fermeture à ' . $closingHourInParams->getValueName() . 'H.',
                    Response::HTTP_BAD_REQUEST, [], false);
            }

        } else {
            return new JsonResponse('La salle n\'est pas disponible le ' . $day, Response::HTTP_BAD_REQUEST, [], false);
        }

        if ($available === 0) {

            $booking = new Booking();
            $room = $roomRepository->find($dataTab['room']);
            $serializerInterface->deserialize($data, Booking::class, 'json', ['object_to_populate' => $booking]);
            $booking->setRoom($room);

            $errors = $validatorInterface->validate($booking);
            if (count($errors)) {
                $errorsJson = $serializerInterface->serialize($errors, 'json');
                return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
            }

            $manager->persist($booking);
            $manager->flush();

            return new JsonResponse("La réservation " . $booking . " a été créée", Response::HTTP_CREATED, [], true);
        }

    }

    /**
     * @Route("/api/booking/{id}", name="api.booking.update", methods={"PUT"}, requirements={"id":"\d+"})
     */
    public function update(Booking $booking, Request $request, BookingRepository $bookingRepository, ParamsRepository $paramsRepository, RoomRepository $roomRepository, DecoderInterface $decoderInterface, SerializerInterface $serializerInterface, EntityManagerInterface $manager, ValidatorInterface $validatorInterface)
    {
        $token = $request->query->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], false);
        }

        $data = $request->getContent();
        $dataTab = $decoderInterface->decode($data, 'json');

        // Check if day is valid

        $date = Helper::getDate($dataTab['date']);
        $day = $date->dayName;
        $dayInParams = $paramsRepository->findOneBy(['keyName' => $day]);

        if ($dayInParams->getValueName() === (string) 1) {

            // Check if hours are valid

            $openingHour = Helper::getHour($dataTab['openingHour']);
            $closingHour = Helper::getHour($dataTab['closingHour']);
            $openingHourInParams = $paramsRepository->findOneBy(['keyName' => 'heure_ouverture']);
            $closingHourInParams = $paramsRepository->findOneBy(['keyName' => 'heure_fermeture']);

            if (($openingHour >= $openingHourInParams->getValueName() && $closingHour <= $closingHourInParams->getValueName()) && $openingHour < $closingHour) {

                // Check if room is available

                $available = 0;
                $bookings = $bookingRepository->findAll();

                // Remove updating booking

                $element = array_search($booking, $bookings);
                unset($bookings[$element]);

                foreach ($bookings as $value) {

                    if ($value->getRoom()->getId() === (int) $dataTab['room']) {

                        $roomDate = Helper::getDate($value->getDate());

                        // Check if dates are equal

                        if ($date->equalTo($roomDate)) {

                            $roomOpeningHour = Helper::getHour($value->getOpeningHour());
                            $roomClosingHour = Helper::getHour($value->getClosingHour());
                            //dd($value->getId(), $booking->getId());

                            if ((($roomOpeningHour <= $openingHour) && ($openingHour <= $roomClosingHour)) || (($roomOpeningHour <= $closingHour) && ($closingHour <= $roomClosingHour))) {
                                $available++;
                                return new JsonResponse('La salle est déjà réservée sur ce créneau.',
                                    Response::HTTP_BAD_REQUEST, [], false);
                            }
                        }
                    }
                }

            } else {
                return new JsonResponse('La salle n\'est pas disponible sur ce créneau : Ouverture à ' . $openingHourInParams->getValueName() . 'H et fermeture à ' . $closingHourInParams->getValueName() . 'H.',
                    Response::HTTP_BAD_REQUEST, [], false);
            }

        } else {
            return new JsonResponse('La salle n\'est pas disponible le ' . $day, Response::HTTP_BAD_REQUEST, [], false);
        }

        if ($available === 0) {

            $room = $roomRepository->find($dataTab['room']);
            $serializerInterface->deserialize($data, Booking::class, 'json', ['object_to_populate' => $booking]);
            $booking->setRoom($room);

            $errors = $validatorInterface->validate($booking);
            if (count($errors)) {
                $errorsJson = $serializerInterface->serialize($errors, 'json');
                return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
            }

            $manager->persist($booking);
            $manager->flush();

            return new JsonResponse("La réservation " . $booking . "a été mise à jour", Response::HTTP_CREATED, [], true);
        }
    }

    /**
     * @Route("/api/booking/{id}", name="api.booking.delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function delete(Request $request, Booking $booking, EntityManagerInterface $manager)
    {
        $token = $request->query->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], false);
        }

        $manager->remove($booking);
        $manager->flush();

        return new JsonResponse("La réservation a bien été supprimé", Response::HTTP_OK, [], false);
    }

}
