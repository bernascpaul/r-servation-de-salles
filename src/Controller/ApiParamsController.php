<?php

namespace App\Controller;

use App\Entity\Params;
use App\Repository\ParamsRepository;
use App\Service\Auth;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ApiParamsController extends AbstractController
{
    /**
     * @Route("/api/params", name="api.params.getAll", methods={"GET"})
     */
    public function getAll(Request $request, ParamsRepository $paramsRepository, SerializerInterface $serializerInterface)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $params = $paramsRepository->findAll();
        $result = $serializerInterface->serialize($params, 'json', [
            'groups' => ['params:simple'],
        ]);

        return new JsonResponse($result, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/params/{id}", name="api.params.getById", methods={"GET"})
     */
    public function getById(Request $request, Params $params, SerializerInterface $serializer)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $data = $serializer->serialize($params, 'json', [
            'groups' => ['params:simple'],
        ]);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/params/{id}", name="api_params_update", methods={"POST"})
     */
    public function UpdateRoom(Request $request, Params $params, DecoderInterface $decoderInterface, SerializerInterface $serializerInterface, EntityManagerInterface $manager, ValidatorInterface $validatorInterface)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $data = $request->getContent();
        $dataTab = $decoderInterface->decode($data, 'json');

        if (array_key_exists('keyName', $dataTab)) {
            $keyName = $dataTab['keyName'];
            $params->setKeyName($keyName);
        }

        if (array_key_exists('valueName', $dataTab)) {
            $valueName = $dataTab['valueName'];
            $params->setValueName($valueName);
        }

        $errors = $validatorInterface->validate($params);
        if (count($errors)) {
            $errorsJson = $serializerInterface->serialize($errors, 'json');
            return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
        }

        $manager->persist($params);
        $manager->flush();

        return $this->json($params, Response::HTTP_CREATED, [], ['groups' => 'room:read']);

    }

    /**
     * @Route("/api/params/{id}", name="api.params.delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function delete(Request $request, Params $params, EntityManagerInterface $manager)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $manager->remove($params);
        $manager->flush();

        return new JsonResponse("Le paramètre a bien été supprimé", Response::HTTP_OK, [], false);
    }

}
