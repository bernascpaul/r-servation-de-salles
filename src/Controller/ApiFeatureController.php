<?php

namespace App\Controller;

use App\Entity\Feature;
use App\Repository\FeatureRepository;
use App\Service\Auth;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ApiFeatureController extends AbstractController
{
    /**
     * @Route("/api/feature", name="api.feature.getAll", methods={"GET"})
     */
    public function getAll(Request $request, FeatureRepository $featureRepository, SerializerInterface $serializerInterface)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $features = $featureRepository->findAll();
        $result = $serializerInterface->serialize($features, 'json', [
            'groups' => ['feature:simple'],
        ]);

        return new JsonResponse($result, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/feature/{id}", name="api.feature.getById", methods={"GET"})
     */
    public function getById(Request $request, Feature $feature, SerializerInterface $serializer)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $data = $serializer->serialize($feature, 'json', [
            'groups' => ['feature:full'],
        ]);

        return new JsonResponse($data, Response::HTTP_OK, [], true);
    }

    /**
     * @Route("/api/feature/{id}", name="api.feature.delete", methods={"DELETE"}, requirements={"id":"\d+"})
     */
    public function delete(Request $request, Feature $feature, EntityManagerInterface $manager)
    {
        $token = $request->headers->get('token');
        if (!Auth::checkAccess($token)) {
            return new JsonResponse('Vous n\'êtes pas authorisé à accèder à ce module ou votre token a expiré.', Response::HTTP_UNAUTHORIZED, [], true);
        }

        $manager->remove($feature);
        $manager->flush();

        return new JsonResponse("L'equipement a bien ete supprime", Response::HTTP_OK, [], false);
    }

}
