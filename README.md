# API de Réservation de salles

Projet Symfony de réservation de salles

# Auteurs

- Paul BERNASCONI
- Pierre GICQUEL
- Fabien LEBRUEILLY
- Romain LE REST

## Installation

Se rendre sur le [projet](https://gitlab.com/bernascpaul/r-servation-de-salles) et récupérer le clone.

```sh
$ git clone https://gitlab.com/bernascpaul/r-servation-de-salles.git
$ cd r-servation-de-salles/
$ docker run --rm --interactive --tty \
  --volume $PWD:/app \
  --user $(id -u):$(id -g) \
  composer install
$ docker-compose up -d
$ docker exec -ti symfony-php bash
$ php bin/console d:m:m
```

Accès:

* View => https://gitlab.com/NamorEsther/reservationsalle-appliangular
* phpMyAdmin => http://{votreadresse}:8889
* Documentation => https://pierre-gicquel77.gitbook.io/api-reservation-salles/


## Ajout de données

```sh
$ docker exec -ti symfony-php bash
$ php bin/console doctrine:fixtures:load
```

## Mise à jour

```sh
$ docker run --rm --interactive --tty \
  --volume $PWD:/app \
  --user $(id -u):$(id -g) \
  composer update
```